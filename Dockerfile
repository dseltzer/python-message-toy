# Use the official Python image from the Docker Hub
FROM python:3-slim

# Set the working directory
WORKDIR /app

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the script into the container
COPY pmt.py .

# Set the entrypoint to our script
ENTRYPOINT ["python", "pmt.py"]