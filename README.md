# Python Message Toy

Python Message Toy (PMT) is an application that uses Python, RabbitMQ, and the Pika library to transmit files and generate mock data for testing throughput.

## Setup

### Dependencies

To run PMT, Python 3.9 or later is required, along with the following Python packages:

- `click`
- `pika`
- `tqdm`

You can install these dependencies using `pip`:

```shell
pip install -r requirements.txt
```

### RabbitMQ Server

You also need a RabbitMQ server. You can set up a local server using Docker:

```shell
docker run -d --name rabbitmq-server -p 5672:5672 -p 15672:15672 registry.gitlab.com/dseltzer/python-message-toy/skinny-rabbit:latest
```

This command will start a RabbitMQ server in a Docker container, and expose it on ports `5672` and `15672`.

## Usage

### Common Options

- `--amqp` - The RabbitMQ URI (default is `amqp://guest:guest@localhost:5672/%2F`)

### Transmit Command

Transmit files from a local directory to a RabbitMQ exchange.

```shell
python pmt.py transmit [OPTIONS] PATH...
```

Options:

- `--filename-regex-filter` - Filter filenames using a regular expression
- `--extension-filter` - Filter by file extension
- `--max-filesize-filter` - Filter out files larger than this value. Examples: (100kb, 2mb, 1gb, 1024000)
- `--exchange` - Configure the output exchange name (always topic)
- `--queue` - Configure the output queue name and bind it
- `--binding` - Configure the link between the exchange and queue
- `--recursive` - Recursively walk through directories
- `--disable-publisher-confirms` - Disable publisher-confirmation to improve throughput (disables backpressure sensing)

Example:

```shell
python pmt.py transmit --filename-regex-filter=".*" --extension-filter=".txt" --exchange="exchange" --queue="queue" /path/to/files
```

You can also run PMT in a Docker container. Here's an example command:

```shell
docker run --link rabbitmq-server \
           --rm -it registry.gitlab.com/dseltzer/python-message-toy/pmt:latest \
           --amqp="amqp://guest:guest@rabbitmq-server:5672/%2f" \
           transmit --filename-regex-filter=".*" --extension-filter=".txt" --exchange="exchange" --queue="queue" /data
```

### Receive Command

Receive messages from a RabbitMQ queue and print the message body to the console.

```shell
python pmt.py receive [OPTIONS]
```

Options:

- `--queue` - Configure the input queue
- `--exchange` - If set, create a topic exchange with this name
- `--binding` - If set and `--exchange` is set, bind the queue to the exchange using this binding
- `--prefetch` - Configure the size of the prefetch
- `--autoack` - Enable auto-acknowledge

Example:

```shell
python pmt.py receive --queue="queue" --exchange="exchange" --binding="#" --prefetch=1 --autoack
```

And the Docker equivalent:

```shell
docker run --link rabbitmq-server \
            --rm -it registry.gitlab.com/dseltzer/python-message-toy/pmt:latest \
            --amqp="amqp://guest:guest@rabbitmq-server:5672/%2f" \
            receive --queue="queue" --exchange="exchange" --binding="#" --prefetch=1 --autoack
```

### Generate Command

Generate mock data and transmit it to RabbitMQ for throughput testing.

```shell
python pmt.py generate [OPTIONS]
```

Options:

- `--size` - The target size of the message in bytes (default is 10 KB)
- `--jitter-percent` - Percentage by which each message may vary in size (default is 10)
- `--naming` - Method to name the fake file (options are 'numbered' and 'random', default is 'numbered')
- `--extension` - File extension for the fake filename (default is '.txt')
- `--exchange` - The output exchange name
- `--queue` - The output queue name
- `--binding` - The link between the exchange and queue
- `--stop-after` - Stop transmitting after this many messages or bytes (e.g. "10000" or "10GB")
- `--disable-publisher-confirms` - Disable publisher-confirmation to improve throughput (disables backpressure sensing)

Example:

```shell
python pmt.py generate --size=10240 --jitter-percent=10 --naming="numbered" --extension=".txt" --exchange="exchange" --queue="queue" --binding="#" --stop-after="10GB
```

And the Docker equivalent:

```shell
docker run --link rabbitmq-server \
    --rm -it registry.gitlab.com/dseltzer/python-message-toy/pmt:latest \
    --amqp="amqp://guest:guest@rabbitmq-server:5672/%2f" \
    generate --size=10240 --jitter-percent=10 --naming="numbered" --extension=".txt" \
             --exchange="exchange" --queue="queue" --binding="#" --stop-after="10GB"
```

## Test Example

![Test Output](images/test-example.gif)

Here's a simple test-session:

```shell
docker run  -dit --rm --name rabbitmq-server \
            -p 5672:5672 -p 15672:15672 \
            registry.gitlab.com/dseltzer/python-message-toy/skinny-rabbit:latest
echo "Waiting for RabbitMQ Boot..."
sleep 30
generator_command=$(cat << 'EOF'
docker run --link rabbitmq-server --name rabbitmq-generator \
            --rm -it registry.gitlab.com/dseltzer/python-message-toy/pmt:latest \
            --amqp="amqp://guest:guest@rabbitmq-server:5672/%2f" \
            generate --size=10240 --jitter-percent=10 --naming="numbered" --extension=".txt" \
                     --exchange="exchange" --queue="queue" --binding="#" 
EOF
)
receiver_command=$(cat << 'EOF'                    
docker run --link rabbitmq-server  --name rabbitmq-receiver \
            --rm -it registry.gitlab.com/dseltzer/python-message-toy/pmt:latest \
            --amqp="amqp://guest:guest@rabbitmq-server:5672/%2f" \
            receive --queue="queue" --exchange="exchange" --binding="#" --prefetch=1 --autoack
EOF
)
echo "Configuring tmux"
tmux new-session -d -s dockerview
# Split the window horizontally
tmux split-window -v -p 50
# Split the bottom pane vertically
tmux select-pane -t 1
tmux split-window -h
# Run docker logs command in each pane
tmux select-pane -t 0
tmux send-keys 'docker logs -f rabbitmq-server' C-m
tmux select-pane -t 1
tmux send-keys "$generator_command" C-m
tmux select-pane -t 2
tmux send-keys "$receiver_command" C-m
# Attach to the session
tmux attach-session -t dockerview
```

To quit tmux simply ctrl-c each window and exit each shell.

**After testing don't forget to clean up!**
```shell
docker stop rabbitmq-server rabbitmq-generator rabbitmq-receiever
```