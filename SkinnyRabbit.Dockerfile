# Start from rabbitmq:3-management
FROM rabbitmq:3-management

# Create the advanced.config file
RUN cat <<EOF > /etc/rabbitmq/conf.d/20-memorycap.conf
vm_memory_high_watermark.absolute = 500MiB
vm_memory_calculation_strategy = allocated
vm_memory_high_watermark_paging_ratio = 0.9999
EOF