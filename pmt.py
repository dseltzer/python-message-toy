import os
import re
import click
import pika
import logging
import random
import string
import time
from tqdm import tqdm

# Set up logging
logging.basicConfig(level=logging.INFO)
logging.getLogger("pika").setLevel(logging.ERROR)


@click.group()
@click.option('--amqp', default='amqp://guest:guest@localhost:5672/%2F?heartbeat=30', help='The RabbitMQ URI')
@click.pass_context
def cli(ctx, amqp):
    ctx.ensure_object(dict)
    ctx.obj['AMQP'] = amqp

def parse_size(size_str):
    size_str = size_str.lower().strip()
    if size_str.endswith('kb'):
        return int(size_str[:-2]) * 1024
    elif size_str.endswith('mb'):
        return int(size_str[:-2]) * 1024 * 1024
    elif size_str.endswith('gb'):
        return int(size_str[:-2]) * 1024 * 1024 * 1024
    else:
        return int(size_str)

def process_file(filepath, filename_regex_filter, extension_filter, channel, exchange, max_filesize, use_publisher_confirms=True):
    fileBase = os.path.basename(filepath)
    if not re.match(filename_regex_filter, fileBase):
        return 0
    if extension_filter and not fileBase.endswith(extension_filter):
        return 0
    if max_filesize is not None and os.stat(filepath).st_size > max_filesize:
        return 0

    with open(filepath, 'rb') as file:
        data = file.read()
        #OLD
        while use_publisher_confirms:
            try:
                channel.basic_publish(exchange=exchange, routing_key=fileBase, body=data, mandatory=True)
                return len(data)
            except (pika.exceptions.UnroutableError, pika.exceptions.NackError):
                time.sleep(0.5)  # Sleep for 500 ms before retrying
        else:
            channel.basic_publish(exchange=exchange, routing_key=fileBase, body=data)
            return len(data)  

    return len(0)

@click.command()
@click.option('--filename-regex-filter', default='.*', help='Regex to filter filenames')
@click.option('--extension-filter', default='', help='File extension to filter')
@click.option('--exchange', default='exchange', help='The output exchange name')
@click.option('--queue', default='queue', help='The output queue name')
@click.option('--binding', default='#', help='The link between the exchange and queue')
@click.option('--recursive', is_flag=True, help='Go into subdirectories recursively')
@click.option('--max-filesize-filter', default=None, help='Skip files larger than this size. E.g. 1MB, 200KB')
@click.option('--disable-publisher-confirms', is_flag=True, default=False, help='Disable publisher confirms')
@click.argument('paths', nargs=-1)
@click.pass_context
def transmit(ctx, filename_regex_filter, extension_filter, exchange, queue, binding, recursive, max_filesize_filter, disable_publisher_confirms, paths):
    logging.info("Connecting to RabbitMQ...")
    connection = pika.BlockingConnection(pika.URLParameters(ctx.obj['AMQP']))
    channel = connection.channel()
    
    logging.info(f"Configuring exchange {exchange}...")
    channel.exchange_declare(exchange=exchange, exchange_type='topic', durable=True)
    if queue:
        logging.info(f"Configuring queue {queue}...")
        result = channel.queue_declare(queue, durable=True)
        channel.queue_bind(exchange=exchange, queue=queue, routing_key=binding)
    
    max_filesize = None
    if max_filesize_filter is not None:
        max_filesize = parse_size(max_filesize_filter)

    if not disable_publisher_confirms:
        channel.confirm_delivery()  # Enable publisher confirms

    with tqdm(unit='B', unit_scale=True) as pbar:
        for path in paths:
            logging.info(f"Transmitting files from {path}...")
            if recursive:
                for root, dirs, files in os.walk(path):
                    for file in files:
                        filepath = os.path.join(root, file)
                        bytes_transmitted = process_file(filepath, filename_regex_filter, extension_filter, channel, exchange, max_filesize, not disable_publisher_confirms)
                        if bytes_transmitted:
                            pbar.update(bytes_transmitted)
            else:
                for file in os.listdir(path):
                    if os.path.isfile(os.path.join(path, file)):
                        filepath = os.path.join(path, file)
                        bytes_transmitted = process_file(filepath, filename_regex_filter, extension_filter, channel, exchange, max_filesize, not disable_publisher_confirms)
                        if bytes_transmitted:
                            pbar.update(bytes_transmitted)
    
    connection.close()


@click.command()
@click.option('--queue', default='queue', help='The input queue name')
@click.option('--exchange', help='The input exchange name')
@click.option('--binding', help='The link between the input exchange and queue')
@click.option('--prefetch', type=int, default=1, help='The prefetch size')
@click.option('--autoack', is_flag=True, default=False, help='Enable autoack')
@click.pass_context
def receive(ctx, queue, exchange, binding, prefetch, autoack):
    logging.info("Connecting to RabbitMQ...")
    connection = pika.BlockingConnection(pika.URLParameters(ctx.obj['AMQP']))
    channel = connection.channel()
    
    if exchange and binding:
        logging.info(f"Configuring exchange {exchange}...")
        channel.exchange_declare(exchange=exchange, exchange_type='topic')
        logging.info(f"Configuring queue {queue}...")
        result = channel.queue_declare(queue, durable=True)
        channel.queue_bind(exchange=exchange, queue=queue, routing_key=binding)
    
    channel.basic_qos(prefetch_count=prefetch)
    
    # Create a tqdm object with an unknown total (it will dynamically adapt as we update it)
    pbar = tqdm(total=None, unit='B', unit_scale=True)

    def on_message(channel, method, properties, body):
        pbar.update(len(body))  # Update the progress bar with the size of the current message
        if not autoack:
            channel.basic_ack(delivery_tag=method.delivery_tag)

    channel.basic_consume(queue=queue, on_message_callback=on_message, auto_ack=autoack)
    
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    connection.close()
    pbar.close()  # Close the tqdm object when we're done


@click.command()
@click.option('--size', default=10*1024, help='The target size of the message in bytes')
@click.option('--jitter-percent', default=10, help='Percentage by which each message may vary in size')
@click.option('--naming', default='numbered', type=click.Choice(['numbered', 'random']), help='Method to name the fake file')
@click.option('--extension', default='.txt', help='File extension for the fake filename')
@click.option('--exchange', default='exchange', help='The output exchange name')
@click.option('--queue', default='queue', help='The output queue name')
@click.option('--binding', default='#', help='The link between the exchange and queue')
@click.option('--stop-after', default=None, help='Stop transmitting after this many messages or bytes (e.g. "10000" or "10GB")')
@click.option('--random-body', is_flag=True, default=False, help='Generate random body content for each message (SLOW)')
@click.option('--disable-publisher-confirms', is_flag=True, default=False, help='Disable publisher confirms')
@click.pass_context
def generate(ctx, size, jitter_percent, naming, extension, exchange, queue, binding, stop_after, random_body, disable_publisher_confirms):
    logging.info("Connecting to RabbitMQ...")
    connection = pika.BlockingConnection(pika.URLParameters(ctx.obj['AMQP']))
    channel = connection.channel()
    
    logging.info(f"Configuring exchange {exchange}...")
    channel.exchange_declare(exchange=exchange, exchange_type='topic')
    if queue:
        logging.info(f"Configuring queue {queue}...")
        result = channel.queue_declare(queue, durable=True, )
        channel.queue_bind(exchange=exchange, queue=queue, routing_key=binding)
    
    if not disable_publisher_confirms:
        channel.confirm_delivery()  # Enable publisher confirms

    content_pattern = "0123456789"
    chars = string.ascii_letters + string.digits
    counter = 1
    total_bytes = 0
    stop_after_bytes = None
    stop_after_messages = None
    
    if stop_after:
        if stop_after.endswith('GB'):
            stop_after_bytes = int(stop_after[:-2]) * 1024 * 1024 * 1024
        else:
            stop_after_messages = int(stop_after)
    
    with tqdm(unit='B', unit_scale=True) as pbar:
        while True:
            # Calculate message size with jitter
            jitter = size * (jitter_percent / 100.0)
            message_size = random.randint(int(size - jitter), int(size + jitter))
            
            # Generate file content
            if random_body:
                # Generate a random string of the specified length
                content = ''.join(random.choice(chars) for _ in range(message_size))
            else:
                # This is faster....
                content = (content_pattern * (message_size // len(content_pattern) + 1))[:message_size]
            
            

            # Generate file name
            if naming == 'numbered':
                file_name = f"{counter:09d}{extension}"
            else:
                file_name = f"{''.join(random.choices(string.ascii_letters, k=9))}{extension}"
            
            # Send message to RabbitMQ
            while not disable_publisher_confirms:
                try:
                    channel.basic_publish(exchange=exchange, routing_key=file_name, body=content, mandatory=True)
                    break
                except (pika.exceptions.UnroutableError, pika.exceptions.NackError):
                    pbar.update(0)
                    time.sleep(0.5)  # Sleep for 500 ms before retrying
            else:
                channel.basic_publish(exchange=exchange, routing_key=file_name, body=content)
            
            # Update progress bar and total bytes
            pbar.update(message_size)
            total_bytes += message_size
            
            # Increment counter
            counter += 1
            
            # Stop if we've reached the limit
            if stop_after_messages is not None and counter > stop_after_messages:
                break
            if stop_after_bytes is not None and total_bytes >= stop_after_bytes:
                break


cli.add_command(transmit)
cli.add_command(receive)
cli.add_command(generate)

if __name__ == "__main__":
    cli(obj={})
